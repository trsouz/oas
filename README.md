# oas [![Build Status](https://secure.travis-ci.org/trsouz/oas.png?branch=master)](https://travis-ci.org/trsouz/oas)

Simple oas parse

## Install

```bash
npm install --save oas
```

## Examples

See `examples/` for different use cases.

## License

MIT

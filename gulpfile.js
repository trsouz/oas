var gulp = require('gulp'),
  mocha = require('gulp-mocha'),
  jshint = require('gulp-jshint'),
  watch = require('gulp-watch'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),
  stylish = require('jshint-stylish'),
  mochaPhantomJS = require('gulp-mocha-phantomjs');

gulp.task('lint', function () {
  'use strict';
  return gulp.src('./lib/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'));
});

gulp.task('test', ['lint'], function () {
  'use strict';
  return gulp.src('./tests/index.js')
    .pipe(mocha({bail: true}));
});

gulp.task('testBrowser', ['lint'], function () {
  'use strict';
  return gulp.src('tests/index.html')
    .pipe(mochaPhantomJS({reporter: 'spec'}));
});

gulp.task('js', ['test'], function () {
  'use strict';
  return gulp.src('./lib/**/*.js')
    .pipe(uglify())
    .pipe(concat('oas.js'))
    .pipe(gulp.dest('build'));
});

gulp.task('watch', function () {
  'use strict';
  gulp.watch(['gulpfile.js', 'lib/**/*', 'tests/**/*'], ['test', 'testBrowser']);
});

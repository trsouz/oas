(function (root) {

  /* OAS Events Map
   * load (collection)
   * load:start
   * load:end
   *
   * print (collection)
   * print:start
   * print:end
   *
   * print:positionName (position, collection)
   *
   * add (position, collection)
   * add:positionName
   *
   * remove (position, collection)
   * remove:positionName
   */

  if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (fn, scope) {
      if (typeof fn === 'function') {
        for (var i = 0, len = this.length; i < len; ++i) {
          fn.call(scope, this[i], i, this);
        }
      }
    };
  }

  var ArrayProto = Array.prototype,
    ObjProto = Object.prototype;

  var breaker = {};
  var slice = ArrayProto.slice,
    toString = ObjProto.toString,
    hasOwnProperty = ObjProto.hasOwnProperty,
    nativeForEach = ArrayProto.forEach,
    nativeIsArray = Array.isArray;

  var document = root.document || {};
  var location = root.location || {};

  var idCounter = 0;

  var isArray = nativeIsArray || function (obj) {
    return toString.call(obj) === '[object Array]';
  };

  function uniqueId(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  }

  function each(obj, iterator, context) {
    if (obj === null) {
      return;
    }
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, l = obj.length; i < l; i++) {
        if (iterator.call(context, obj[i], i, obj) === breaker) {
          return;
        }
      }
    } else {
      for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) {
          if (iterator.call(context, obj[key], key, obj) === breaker) {
            return;
          }
        }
      }
    }
  }

  // Extend a given object with all the properties in passed-in object(s).
  function extend(obj) {
    each(slice.call(arguments, 1), function (source) {
      if (source) {
        for (var prop in source) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  }

  function loadScript(url, callback) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    script.onreadystatechange = callback;
    script.onload = callback;

    // fire the loading
    head.appendChild(script);
  }

  /*function onLoad(func) {
    if (root.addEventListener) {
      root.addEventListener("load", func, false);
    } else if (document.addEventListener) {
      document.addEventListener("load", func, false);
    } else if (root.attachEvent) {
      root.attachEvent("onload", func);
    } else if (typeof root.onload !== "function") {
      root.onload = func;
    } else {
      var oldonload = root.onload;
      root.onload = function () {
        oldonload();
        func();
      };
    }
  }*/

  function getPositionsCall() {
    var map = {};

    function getPositionsCallEach(value) {
      var position = value;
      position = (position.indexOf("OAS_AD('") !== -1) ? position.split('OAS_AD(\'') : position.split('OAS_AD("');
      if (position.length > 1) {
        position = (position[1].indexOf("')") !== -1) ? position[1].split('\')') : position[1].split('"');
        position = position[0];
        map[position] = map[position] || [];
        map[position].push(current);
      }
    }

    for (var i = 0; i < document.scripts.length; i++) {
      var current = document.scripts[i];
      if (!current.getAttribute('src')) {
        var positions = current.innerHTML.match(/OAS_AD(\(.*\)*)/gi) || [];
        positions.forEach(getPositionsCallEach);
      }
    }
    return map;
  }

  function has(obj, key) {
    return hasOwnProperty.call(obj, key);
  }

  function keys(obj) {
    if (obj !== Object(obj)) {
      throw new TypeError('Invalid object');
    }
    var k = [];
    for (var key in obj) {
      if (has(obj, key)) {
        k[k.length] = key;
      }
    }
    return k;
  }

  // Events
  // ---------------

  // A module that can be mixed in to *any object* in order to provide it with
  // custom events. You may bind with `on` or remove with `off` callback
  // functions to an event; `trigger`-ing an event fires all callbacks in
  // succession.
  //
  //     var object = {};
  //     _.extend(object, Backbone.Events);
  //     object.on('expand', function(){ alert('expanded'); });
  //     object.trigger('expand');
  //
  var Events = {

    // Bind an event to a `callback` function. Passing `"all"` will bind
    // the callback to all events fired.
    on: function (name, callback, context) {
      if (!eventsApi(this, 'on', name, [callback, context]) || !callback) {
        return this;
      }
      this._events = this._events || {};
      var events = this._events[name] || (this._events[name] = []);
      events.push({callback: callback, context: context, ctx: context || this});
      return this;
    },

    // Bind an event to only be triggered a single time. After the first time
    // the callback is invoked, it will be removed.
    once: function (name, callback, context) {
      if (!eventsApi(this, 'once', name, [callback, context]) || !callback) {
        return this;
      }
      var self = this;
      var once = once(function () {
        self.off(name, once);
        callback.apply(this, arguments);
      });
      once._callback = callback;
      return this.on(name, once, context);
    },

    // Remove one or many callbacks. If `context` is null, removes all
    // callbacks with that function. If `callback` is null, removes all
    // callbacks for the event. If `name` is null, removes all bound
    // callbacks for all events.
    off: function (name, callback, context) {
      var retain, ev, events, names, i, l, j, k;
      if (!this._events || !eventsApi(this, 'off', name, [callback, context])) {
        return this;
      }
      if (!name && !callback && !context) {
        this._events = {};
        return this;
      }

      names = name ? [name] : keys(this._events);
      for (i = 0, l = names.length; i < l; i++) {
        name = names[i];
        if (events === this._events[name]) {
          this._events[name] = retain = [];
          if (callback || context) {
            for (j = 0, k = events.length; j < k; j++) {
              ev = events[j];
              if ((callback && callback !== ev.callback && callback !== ev.callback._callback) ||
                (context && context !== ev.context)) {
                retain.push(ev);
              }
            }
          }
          if (!retain.length) {
            delete this._events[name];
          }
        }
      }

      return this;
    },

    // Trigger one or many events, firing all bound callbacks. Callbacks are
    // passed the same arguments as `trigger` is, apart from the event name
    // (unless you're listening on `"all"`, which will cause your callback to
    // receive the true name of the event as the first argument).
    trigger: function (name) {
      if (!this._events) {
        return this;
      }
      var args = slice.call(arguments, 1);
      if (!eventsApi(this, 'trigger', name, args)) {
        return this;
      }
      var events = this._events[name];
      var allEvents = this._events.all;
      if (events) {
        triggerEvents(events, args);
      }
      if (allEvents) {
        triggerEvents(allEvents, arguments);
      }
      return this;
    },

    // Tell this object to stop listening to either specific events ... or
    // to every object it's currently listening to.
    stopListening: function (obj, name, callback) {
      var listeners = this._listeners;
      if (!listeners) {
        return this;
      }
      var deleteListener = !name && !callback;
      if (typeof name === 'object') {
        callback = this;
      }
      if (obj) {
        (listeners = {})[obj._listenerId] = obj;
      }
      for (var id in listeners) {
        listeners[id].off(name, callback, this);
        if (deleteListener) {
          delete this._listeners[id];
        }
      }
      return this;
    }

  };

  // Regular expression used to split event strings.
  var eventSplitter = /\s+/;

  // Implement fancy features of the Events API such as multiple event
  // names `"change blur"` and jQuery-style event maps `{change: action}`
  // in terms of the existing API.
  var eventsApi = function (obj, action, name, rest) {
    if (!name) {
      return true;
    }

    // Handle event maps.
    if (typeof name === 'object') {
      for (var key in name) {
        obj[action].apply(obj, [key, name[key]].concat(rest));
      }
      return false;
    }

    // Handle space separated event names.
    if (eventSplitter.test(name)) {
      var names = name.split(eventSplitter);
      for (var i = 0, l = names.length; i < l; i++) {
        obj[action].apply(obj, [names[i]].concat(rest));
      }
      return false;
    }

    return true;
  };

  // A difficult-to-believe, but optimized internal dispatch function for
  // triggering events. Tries to keep the usual cases speedy (most internal
  // Backbone events have 3 arguments).
  var triggerEvents = function (events, args) {
    var ev, i = -1, l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
    switch (args.length) {
      case 0:
        while (++i < l) {
          (ev = events[i]).callback.call(ev.ctx);
        }
        return;
      case 1:
        while (++i < l) {
          (ev = events[i]).callback.call(ev.ctx, a1);
        }
        return;
      case 2:
        while (++i < l) {
          (ev = events[i]).callback.call(ev.ctx, a1, a2);
        }
        return;
      case 3:
        while (++i < l) {
          (ev = events[i]).callback.call(ev.ctx, a1, a2, a3);
        }
        return;
      default:
        while (++i < l) {
          (ev = events[i]).callback.apply(ev.ctx, args);
        }
    }
  };

  var listenMethods = {listenTo: 'on', listenToOnce: 'once'};

  // Inversion-of-control versions of `on` and `once`. Tell *this* object to
  // listen to an event in another object ... keeping track of what it's
  // listening to.
  each(listenMethods, function (implementation, method) {
    Events[method] = function (obj, name, callback) {
      var listeners = this._listeners || (this._listeners = {});
      var id = obj._listenerId || (obj._listenerId = uniqueId('l'));
      listeners[id] = obj;
      if (typeof name === 'object') {
        callback = this;
      }
      obj[implementation](name, callback, this);
      return this;
    };
  });

  function Position(name) {
    return extend(this, {
      name: name,
      content: '',
      nodes: [],
      printed: false,
      update: function (content, nodes) {
        if (typeof content === 'undefined') {
          return false;
        }
        this.content = content;
        this.reference = (nodes || [])[0];
      }
    });
  }

  Position.prototype.container = {};
  Position.prototype.print = function () {
    this.trigger('print:start', this);
    if (this.el) {
      this.el.parentNode.removeChild(this.el);
    }
    var content = this.content;
    this.el = document.createElement("div");
    this.el.className = 'app-oas oas-pos-' + this.name.toLowerCase();

    if (typeof content === 'string') {
      this.el.insertAdjacentHTML("afterbegin", content);
    } else if (content.nodeName) {
      this.appendChild(content);
    }

    if (this.reference && this.el) {
      if (this.reference.nextSibling) {
        this.reference.parentNode.insertBefore(this.el, this.reference.nextSibling);
      } else {
        this.reference.parentNode.appendChild(this.el);
      }
    }
    this.container.trigger('print:' + this.name, this);
    this.trigger('print:end', this);
    this.printed = true;
    return this;
  };

  function OAS() {
    var self = this,
      loaded = false,
      map = {},
      timer;

    function parse(cb) {
      if (!loaded) {
        if (typeof root.OAS_RICH === 'function') {
          loaded = true;
          root.clearTimeout(timer);
          map = getPositionsCall();

          var currentWrite = root.document.write;
          self.forEach(function (value) {
            var content = [];
            root.document.write = function (str) {
              content.push(str);
            };
            root.OAS_RICH(value.name);
            value.update(content.join(''), map[value.name]);
          });

          root.document.write = currentWrite;
          root.OAS_RICH = '';
          cb.call(self);
        } else {
          var a = arguments;
          timer = setTimeout(function () {
            parse.apply(parse, a);
          }, 200);
        }
      }
    }

    return extend(this, {
      load: function (pos) {
        loaded = false;
        var href = pos ? this.href(pos) : this.href();
        if (href) {
          this.trigger('load:start', self);
          if (pos) {
            this.trigger('load:start:' + pos, pos, self);
          }
          loadScript(href, function () {
            parse.call(parse, function () {
              if (pos) {
                this.trigger('load:' + pos, pos, self);
              }
              self.trigger('load', self);
              if (pos) {
                this.trigger('load:end' + pos, pos, self);
              }
              self.trigger('load:end', self);
            });
          });
        }
        return this;
      }
    });
  }

  var OasQuery = 'OAS_query',
    OasUrl = 'OAS_url',
    OasSitepage = 'OAS_sitepage';

  OAS.prototype.length = 0;
  OAS.prototype.query = root[OasQuery] || '';
  OAS.prototype.sitepage = root[OasSitepage] || '';
  OAS.prototype.container = [];

  OAS.prototype.href = function (_pos) {
    var pos = this.get(_pos),
      rand = (Math.random() + '').substring(2, 11);
    if (pos || !_pos) {
      return [
        this.url,
        this.sitepage,
        '/1',
        rand,
        '@',
        (pos ? pos.name : this.join(',')),
        '?',
        this.query
      ].join('');
    }
    return null;
  };

  OAS.prototype.url = (function () {
    if (!root[OasUrl]) {
      var host = location.host || '',
        hostRt;
      if (host.indexOf('americanas.com.br') !== -1) {
        hostRt = 'americanas';
      } else if (host.indexOf('submarino.com.br') !== -1) {
        hostRt = 'submarino';
      } else if (host.indexOf('shoptime.com.br') !== -1) {
        hostRt = 'shoptime';
      } else if (host.indexOf('soubarato.com.br') !== -1) {
        hostRt = 'soubarato';
      } else if (hostRt) {
        return 'http://oas.' + hostRt + '.com.br/RealMedia/ads/adstream_mjx.ads/';
      }
    } else {
      return root[OasUrl];
    }
    return '';
  })();

  OAS.prototype.version = (function () {
    if (root.navigator && (root.navigator.userAgent.indexOf('Mozilla/3') !== -1 || root.navigator.userAgent.indexOf('Mozilla/4.0 WebTV') !== -1)) {
      return 10;
    }
    return 11;
  })();

  //TODO: verificar por parametro objeto
  OAS.prototype.get = function (pos) {
    if (pos) {
      for (var i = 0; i < this.length; i++) {
        if (pos === this[i].name) {
          return this[i];
        }
      }
    }
    return null;
  };

  OAS.prototype.pluck = function (attr) {
    var rt = [];
    this.forEach(function (value) {
      if (value[attr]) {
        rt.push(value[attr]);
      }
    });
    return rt;
  };

  OAS.prototype.join = function (divisor) {
    var rt = '';
    this.forEach(function (value, index, all) {
      rt += value.name + (index < all.length - 1 ? divisor : '');
    });
    return rt;
  };

  //TODO: deve ser adicionado
  OAS.prototype.normal = function () {
    //root.document.write('<A HREF="' + url + 'click_nx.ads/' + sitepage + '/1' + rns + '@' + listpos + '!' + pos + query + '" TARGET=_top>');
    //root.document.write('<IMG SRC="' + url + 'adstream_nx.ads/' + sitepage + '/1' + rns + '@' + listpos + '!' + pos + query + '" BORDER=0></A>');
  };

  OAS.prototype.add = function (pos) {
    if (pos && typeof(pos) === 'string') {
      var p = this.get(pos);
      if (!p) {
        p = new Position(pos);
        p.container = this;
        this.push(p);
      }
      this.trigger('add', p, this);
      this.trigger('add:' + p.name, p, this);
      return p;
    }
    return null;
  };

  var timer = false;
  OAS.prototype.print = function (_pos) {
    var self = this;

    switch (typeof _pos) {
      case 'string' :
        self.trigger('print:start', self);
        var pos = this.get(_pos);
        if (pos) {
          pos.print();
        }
        break;

      case 'object':
        if (isArray(_pos)) {
          _pos.forEach(function (name) {
            self.print(name);
          });
        }
        break;

      case 'undefined':
        self.forEach(function (obj) {
          self.print(obj.name);
        });
        break;
    }
    if (!timer) {
      setTimeout(function () {
        timer = false;
        self.trigger('print:end', self);
      }, 200);
    }
    timer = true;
    return this;
  };

  // Adicionando funcionalidades Array
  ['push',
    'forEach',
    'slice',
    'concat',
    'map',
    'reduce',
    'reduceRight',
    'filter',
    'evsomeery',
    'some',
    'indexOf',
    'lastIndexOf',
    'pop'].forEach(function (value) {
      OAS.prototype[value] = function () {
        return Array.prototype[value].apply(this, arguments);
      };
    }
  );

  extend(Position.prototype, Events);
  extend(OAS.prototype, Events);

  root.oas = new OAS();
  root.OAS = OAS;

  root.OAS_AD = root.OAS_NORMAL = function () {
    root.oas.add.apply(root.oas, arguments);
  };

  root.handleException = function (e) {
    try {
      console.error(e.message);
    } catch (e) {
    }
  };

  /*onLoad(function () {
    root.oas.load().on('load', function () {
      this.print();
    });
  });
  */
})(this);